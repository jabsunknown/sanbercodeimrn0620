//No 1 Animal class
//Release 0

class Animal {
    constructor (name) {
        this.sheep = name;
        this.legs = 4;
        this.cold_blooded = false;
    }get name() {
        return this.sheep;
    }
}
 
var sheep = new Animal ("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log();

//Release 1

class Ape extends Animal{
    constructor (name) {
        super(name);
        this.legs = 2;
    } yell(){
        console.log("Aauooo");
    }
}
class Frog extends Animal{
    constructor (name){
        super(name)
    } jump(){
        console.log("hop hop")}
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log();

//No 2 Function to class
/*
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  

  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
*/

  class Clock {
    constructor ({template}){
        this.template = template;
        this.timer = 0;
        this.output ;
    } render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        this.output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
          console.log(this.output);
        }
        stop() {
          clearInterval(this.timer);
        };
  
      start(){
          this.render();
          this.timer = setInterval(this.render.bind(this), 1000);
        };
  }
  


var clock = new Clock({template: 'h:m:s'});
clock.start();  