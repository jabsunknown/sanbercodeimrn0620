//No 1 Array To Object

var a=[["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]] ;
function arrayToObject(arr){
	var x={};
	var no=1;
	var thisYear = new Date().getFullYear();
	for(var a=0;a<=arr.length-1;a++){
		if(arr[a][3]>thisYear){z="Invalid birth year";}else{z=thisYear-arr[a][3];}
		x.firstName=arr[a][0];
		x.lastName=arr[a][1];
		x.gender=arr[a][2];
		x.age=z;
		console.log(no+". "+arr[a][0]+" "+arr[a][1]+": ",x);
	no++;}
	return "";
}
console.log(arrayToObject(a));

console.log();

//No 2 Shopping time

function shoppingTime(memberId, money) {
    var storeX = {
      "Sepatu Stacattu": 1500000,
      "Baju Zoro": 500000,
      "Baju H&N": 250000,
      "Sweater Uniklooh": 175000,
      "Casing Handphone": 50000,
    };
  
    if (memberId == null || memberId === "") {
      return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else {
      if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
      } else {
        var prices = Object.values(storeX).sort();
        var listPurchased = [];
        var total = 0;
  
        for (var x = 0; x < prices.length; x++) {
          if (prices[x] < money) {
            total += prices[x];
            listPurchased.push(
              Object.keys(storeX)[Object.values(storeX).indexOf(prices[x])]
            );
          }
        }
  
        var result = {
          memberId: memberId,
          money: money,
          listPurchased: listPurchased,
          changeMoney: money - total,
        };
  
        return result;
      }
    }
  }
  
  // TEST CASES
  console.log(shoppingTime("1820RzKrnWn08", 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
  console.log(shoppingTime("82Ku8Ma742", 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  console.log();
  
//No 3  Naik Angkot

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  var object ={}
  for (var i=0; i<arrPenumpang.length; i++) {
    var namaInput = arrPenumpang[i][0]
    var lokasiAwal = arrPenumpang[i][1]
    var lokasiAkhir = arrPenumpang[i][2]
    
    object[namaInput] = {
      penumpang: namaInput,
      naikDari: lokasiAwal,
      tujuan: lokasiAkhir,
      harga: 0
    }
    for (var j=0; j<rute.length; j++){
    if (lokasiAwal === rute[j]) {
      var awal = j
    } else if (lokasiAkhir === rute[j]) {
      var akhir = j
    }
    }
    object[namaInput].harga = Math.abs(2000 * (akhir - awal))
  }
  var values = Object.values(object)
  return values
}
  


//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'D'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
