//No. 1 Range

function range(starNum, finishNum) {
  let array = [];
  let aa = 0;
  if (starNum <= finishNum) {
    for (let a = starNum; a <= finishNum; a++) {
      array[aa] = a;
      aa++;
    }
  } else if (starNum >= finishNum) {
    for (let b = starNum; b >= finishNum; b--) {
      array[aa] = b;
      aa++;
    }
  } else {
    array = -1;
  }
  return array;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

console.log();

//No. 2 Range with Step

function rangeWithStep(starNum, finishNum, step) {
  let array = [];
  let aa = 0;
  if (starNum <= finishNum) {
    for (let a = starNum; a <= finishNum; a += step) {
      array[aa] = a;
      aa++;
    }
  } else if (starNum >= finishNum) {
    for (let b = starNum; b >= finishNum; b -= step) {
      array[aa] = b;
      aa++;
    }
  } else {
    array = -1;
  }
  return array;
}

console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

console.log();

//No. 3 Sum of Range

function sum(starNum, finishNum, step) {
  let sum = 0;

  if (starNum == null) {
    sum = 0;
  } else if (finishNum == null) {
    sum += starNum;
  } else if (step == null) {
    let array = rangeWithStep(starNum, finishNum, 1);
    for (let i = 0; i < array.length; i++) {
      sum += array[i];
    }
  } else {
    let array = rangeWithStep(starNum, finishNum, step);
    for (let i = 0; i < array.length; i++) {
      sum += array[i];
    }
  }
  return sum;
}

console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

console.log();

//No. 4 Array Multidimensi

function dataHandling(input) {
    for (var i=0; i < input.length; i++){
        console.log("Nomor ID: "+input[i][0]);
        console.log("Nama Lengkap: "+input[i][1]);
        console.log("TTL: "+input[i][2]+" "+input[i][3]);
        console.log("Hobi: "+input[i][4]);
        console.log("");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

dataHandling(input);

console.log();

//No. 5 Balik Kata


function balikKata(teks) {
  teks_terbalik = '';
  for(var i=teks.length-1; i>=0; i--) {
      teks_terbalik += teks[i];
  }

  return teks_terbalik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log();

//No. 6 Metode Array

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input=[]){
    input.splice(1,1,input[1]+" Elsharawy");
    input.splice(2,1,"Provinsi "+ input[2]);
    input.splice(4,1,"Pria")
    input.splice(5,0,"SMA Internasional Metro")
    console.log(input);

    var tanggal=input[3].split("/");
    switch(tanggal[1]) {
        case '01':   { console.log('Januari'); break; }
        case '02':   { console.log('Pebruari'); break; }
        case '03':   { console.log('Maret'); break; }
        case '04':   { console.log('April'); break; }
        case '05':   { console.log('Mei'); break; }
        case '06':   { console.log('Juni'); break; }
        case '07':   { console.log('Juli'); break; }
        case '08':   { console.log('Agustus'); break; }
        case '09':   { console.log('September'); break; }
        case '10':   { console.log('Oktober'); break; }
        case '11':   { console.log('November'); break; }
        case '12':   { console.log('Desember'); break; }
        default:  { break; }
    }

    tanggal.sort(function(a, b){return b-a}); 
    console.log(tanggal);
    console.log(input[3].split("/").join('-'));
    console.log(input[1].slice(0,15));

}

dataHandling2(input);
