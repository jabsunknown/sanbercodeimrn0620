// B. TUGAS CONDITIONAL

// soal If-else

var nama = "Junaedi"
var peran = "Werewolf"

// Output untuk Input nama = '' dan peran = ''
if ( nama !== "John" ) {
    console.log("Nama harus diisi!") }
    else {console.log("Selamat datang di dunia werewolf John")}

//Output untuk Input nama = 'John' dan peran = ''
if (peran !== "Samurai") {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else {console.log("Selamat datang Samurai John, bunuh para werewolf itu!")}
 console.log()
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
if ( nama !== "Jane") {
    console.log("Nama harus diisi!") }
    else {
        console.log("Selamat datang di Dunia Werewolf, Jane")
}
if ( peran !== "Penyihir" ) {
    console.log("Halo Jane, Pilih peranmu untuk memulai game!") }
    else {
        console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
 console.log()
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
if ( nama !== "Jenita") {
    console.log("Nama harus diisi!") }
    else {
        console.log("Selamat datang di Dunia Werewolf, Jenita")
}
if ( peran !== "Guard" ) {
    console.log("Halo Jenita, Pilih peranmu untuk memulai game!") }
    else {
        console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
console.log()
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
if ( nama !== "Junaedi") {
    console.log("Nama harus diisi!") }
    else {
        console.log("Selamat datang di Dunia Werewolf, Junaedi")
}
if ( peran !== "Werewolf" ) {
    console.log("Halo Junaedi, Pilih peranmu untuk memulai game!") }
    else {
        console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}

console.log()

//soal switch case

var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1900; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch (bulan){
    case 1: {console.log( tanggal + ' Januari ' + tahun );break;}
    case 2: {console.log( tanggal + ' Februari ' + tahun );break;}
    case 3: {console.log( tanggal + ' Maret ' + tahun );break;}
    case 4: {console.log( tanggal + ' April ' + tahun );break;}
    case 5: {console.log( tanggal + ' Mei ' + tahun );break;}
    case 6: {console.log( tanggal + ' Juni ' + tahun );break;}
    case 7: {console.log( tanggal + ' July ' + tahun );break;}
    case 8: {console.log( tanggal + ' Agustus ' + tahun );break;}
    case 9: {console.log( tanggal + ' September ' + tahun );break;}
    case 10: {console.log( tanggal + ' Oktober ' + tahun );break;}
    case 11: {console.log( tanggal + ' November ' + tahun );break;}
    case 12: {console.log( tanggal + ' Desember ' + tahun );break;}
  }